#include <stdlib.h>
#include <string.h>
#include "proj2sorter.h"
#include "proj2sorter_impl.h"

/* A basic strategy to choose a pivot is to have the root broadcast its
 * median entry, and hope it will be close to the median for all processes */
static int ChoosePivot(Proj2Sorter sorter, MPI_Comm comm, size_t numKeysLocal, uint64_t *keys, uint64_t *pivot_p)
{
  int      err;
  uint64_t pivot = 0;

  if (numKeysLocal) {
    pivot = keys[numKeysLocal / 2];
  }
  err = MPI_Bcast(&pivot, 1, MPI_UINT64_T, 0, comm); MPI_CHK(err);
  *pivot_p = pivot;
  return 0;
}

/* instead of finding a perfect match, use this comparison operation to find
 * when a key fits between two entries in an array */
static int uint64_compare_pair(const void *key, const void *array)
{
  uint64_t Key = *((const uint64_t *) key);
  const uint64_t *pair = (const uint64_t *) array;
  if (Key < pair[0]) return -1;
  if (Key < pair[1]) return 0;
  return 1;
}

static int Proj2SorterSort_quicksort_recursive(Proj2Sorter sorter, MPI_Comm comm, size_t numKeysLocal, uint64_t *keys)
{
  uint64_t pivot = 0;
  uint64_t *lower_half, *upper_half;
  size_t   lower_size, upper_size;
  size_t   numKeysLocalNew = 0;
  uint64_t *keysNew = NULL;
  int      color;
  int      size, rank;
  int      equivRank;
  MPI_Comm subcomm;
  int      err;

  err = MPI_Comm_size(comm, &size); PROJ2CHK(err);
  err = MPI_Comm_rank(comm, &rank); PROJ2CHK(err);
  /* sort locally up front */
  err = Proj2SorterSortLocal(sorter, numKeysLocal, keys, PROJ2SORT_FORWARD); PROJ2CHK(err);
  if (size == 1) {
    /* base case: nothing to do */
    return 0;
  }
  err = ChoosePivot(sorter, comm, numKeysLocal, keys, &pivot); PROJ2CHK(err);
  lower_half = NULL;
  upper_half = NULL;
  lower_size = 0;
  upper_size = 0;
  /* split the keys into those less than and those greater than the pivot */
  if (numKeysLocal) {
    if (pivot < keys[0]) { /* if the pivot is less than the first entry, they all are in the upper half */
      upper_half = keys;
      upper_size = numKeysLocal;
    } else if (pivot >= keys[numKeysLocal - 1]) { /* if the pivot is greater than or equal to the last entry, they all are in the lower half */
      lower_half = keys;
      lower_size = numKeysLocal;
    } else { /* otherwise use a binary search to split the keys */
      lower_half = keys;
      upper_half = (uint64_t *) bsearch(&pivot, keys, numKeysLocal - 1, sizeof(uint64_t), uint64_compare_pair);
      if (!upper_half) PROJ2ERR(MPI_COMM_SELF,5,"Could not find pivot\n");
      /* we have found the greatest entry less than the pivot: increase by one
       * to makek upper_half the start of the entries greater than or equal to
       * the pivot */
      upper_half++;
      lower_size = upper_half - lower_half;
      upper_size = numKeysLocal - lower_size;
    }
  }

  /* color the upper half to split the communicator */
  color = (rank >= (size / 2));
  equivRank = color ? (rank - (size / 2)) : (rank + (size / 2));
  if ((size % 2) == 0 || (rank != size - 1)) {
    /* Easy case: matched send and receive */
    MPI_Request sendreq;
    MPI_Status  recvstatus;

    if (equivRank > rank) {
      int numIncoming;

      err = MPI_Isend(upper_half, (int) upper_size, MPI_UINT64_T, equivRank, PROJ2TAG_QUICKSORT, comm, &sendreq); MPI_CHK(err);
      err = MPI_Probe(equivRank, PROJ2TAG_QUICKSORT, comm, &recvstatus); MPI_CHK(err);
      err = MPI_Get_count(&recvstatus, MPI_UINT64_T, &numIncoming); MPI_CHK(err);
      numKeysLocalNew = lower_size + numIncoming;
      err = Proj2SorterGetWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
      memcpy (keysNew, lower_half, lower_size * sizeof(*keysNew));
      err = MPI_Recv(&keysNew[lower_size], numIncoming, MPI_UINT64_T, equivRank, PROJ2TAG_QUICKSORT, comm, MPI_STATUS_IGNORE); PROJ2CHK(err);
      err = MPI_Wait(&sendreq, MPI_STATUS_IGNORE); MPI_CHK(err);
    } else {
      int numIncoming;

      err = MPI_Isend(lower_half, (int) lower_size, MPI_UINT64_T, equivRank, PROJ2TAG_QUICKSORT, comm, &sendreq); MPI_CHK(err);
      err = MPI_Probe(equivRank, PROJ2TAG_QUICKSORT, comm, &recvstatus); MPI_CHK(err);
      err = MPI_Get_count(&recvstatus, MPI_UINT64_T, &numIncoming); MPI_CHK(err);
      numKeysLocalNew = numIncoming + upper_size;
      err = Proj2SorterGetWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
      memcpy (&keysNew[numIncoming], upper_half, upper_size * sizeof(*keysNew));
      err = MPI_Recv(keysNew, numIncoming, MPI_UINT64_T, equivRank, PROJ2TAG_QUICKSORT, comm, MPI_STATUS_IGNORE); PROJ2CHK(err);
      err = MPI_Wait(&sendreq, MPI_STATUS_IGNORE); MPI_CHK(err);
    }
  }
  if (size % 2) { /* odd number of processes: size - 1 has been left out */
    MPI_Request sendreq;
    MPI_Status  recvstatus;

    /* (size - 1) sends its lower half to (size / 2) - 1 */
    if (rank == (size / 2) - 1) {
      int numIncoming;
      size_t numKeysLocalNewNew;
      uint64_t *keysNewNew;

      /* concatenate the addition from (size - 1) to the already constructed
       * keysNew */
      err = MPI_Probe(size - 1, PROJ2TAG_QUICKSORT, comm, &recvstatus); MPI_CHK(err);
      err = MPI_Get_count(&recvstatus, MPI_UINT64_T, &numIncoming); MPI_CHK(err);
      numKeysLocalNewNew = numKeysLocalNew + numIncoming;
      err = Proj2SorterGetWorkArray(sorter, numKeysLocalNewNew, sizeof(uint64_t), &keysNewNew); PROJ2CHK(err);
      memcpy (keysNewNew, keysNew, numKeysLocalNew * sizeof(*keysNewNew));
      err = Proj2SorterRestoreWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
      err = MPI_Recv(&keysNewNew[numKeysLocalNew], numIncoming, MPI_UINT64_T, size - 1, PROJ2TAG_QUICKSORT, comm, MPI_STATUS_IGNORE); PROJ2CHK(err);
      numKeysLocalNew = numKeysLocalNewNew;
      keysNew = keysNewNew;
    } else if (rank == (size - 1)) {
      /* (size - 1) does not receive from anyone */
      err = MPI_Isend(lower_half, (int) lower_size, MPI_UINT64_T, (size / 2) - 1, PROJ2TAG_QUICKSORT, comm, &sendreq); MPI_CHK(err);
      numKeysLocalNew = upper_size;
      err = Proj2SorterGetWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
      memcpy (keysNew, upper_half, upper_size * sizeof(*keysNew));
      err = MPI_Wait(&sendreq, MPI_STATUS_IGNORE); MPI_CHK(err);
    }
  }
  err = MPI_Comm_split(comm, color, rank, &subcomm); PROJ2CHK(err);
  err = Proj2SorterSort_quicksort_recursive(sorter, subcomm, numKeysLocalNew, keysNew); PROJ2CHK(err);
  err = MPI_Comm_free(&subcomm); PROJ2CHK(err);

  /* Now the array is sorted, but we have to move it back to its original
   * distribution */
  {
    uint64_t myOldCount = numKeysLocal;
    uint64_t myNewCount = numKeysLocalNew;
    uint64_t *oldOffsets;
    uint64_t *newOffsets;
    uint64_t oldOffset, newOffset;
    MPI_Request *recv_reqs, *send_reqs;
    int firstRecv = -1;
    int lastRecv = -2;
    int firstSend = -1;
    int lastSend = -2;
    int nRecvs, nSends;
    uint64_t thisOffset;

    err = Proj2SorterGetWorkArray(sorter, size + 1, sizeof(uint64_t), &oldOffsets); PROJ2CHK(err);
    err = Proj2SorterGetWorkArray(sorter, size + 1, sizeof(uint64_t), &newOffsets); PROJ2CHK(err);
    err = MPI_Allgather(&myOldCount,1,MPI_UINT64_T,oldOffsets,1,MPI_UINT64_T,comm); PROJ2CHK(err);
    err = MPI_Allgather(&myNewCount,1,MPI_UINT64_T,newOffsets,1,MPI_UINT64_T,comm); PROJ2CHK(err);

    oldOffset = 0;
    for (int i = 0; i < size; i++) {
      uint64_t count = oldOffsets[i];
      oldOffsets[i] = oldOffset;
      oldOffset += count;
    }
    oldOffsets[size] = oldOffset;

    newOffset = 0;
    for (int i = 0; i < size; i++) {
      uint64_t count = newOffsets[i];
      newOffsets[i] = newOffset;
      newOffset += count;
    }
    newOffsets[size] = newOffset;

    if (myOldCount) {
      for (int i = 0; i < size; i++) {
        if (newOffsets[i] <= oldOffsets[rank] && oldOffsets[rank] < newOffsets[i + 1]) {
          firstRecv = i;
        }
        if (newOffsets[i] <= oldOffsets[rank + 1] - 1 && oldOffsets[rank + 1] - 1 < newOffsets[i + 1]) {
          lastRecv = i + 1;
          break;
        }
      }
    }

    err = Proj2SorterGetWorkArray(sorter, lastRecv - firstRecv, sizeof(*recv_reqs), &recv_reqs); PROJ2CHK(err);

    thisOffset = oldOffsets[rank];
    nRecvs = 0;
    for (int i = firstRecv; i < lastRecv; i++) {
      size_t recvStart = thisOffset;
      size_t recvEnd   = newOffsets[i + 1];

      if (oldOffsets[rank + 1] < recvEnd) {
        recvEnd = oldOffsets[rank + 1];
      }
      if (recvEnd > recvStart) {
        err = MPI_Irecv(&keys[thisOffset - oldOffsets[rank]],(int) (recvEnd - recvStart), MPI_UINT64_T, i, PROJ2TAG_QUICKSORT, comm, &recv_reqs[nRecvs++]); MPI_CHK(err);
      }
      thisOffset = recvEnd;
    }

    if (myNewCount) {
      for (int i = 0; i < size; i++) {
        if (oldOffsets[i] <= newOffsets[rank] && newOffsets[rank] < oldOffsets[i + 1]) {
          firstSend = i;
        }
        if (oldOffsets[i] <= newOffsets[rank + 1] - 1 && newOffsets[rank + 1] - 1 < oldOffsets[i + 1]) {
          lastSend = i + 1;
          break;
        }
      }
    }

    err = Proj2SorterGetWorkArray(sorter, lastSend - firstSend, sizeof(*send_reqs), &send_reqs); PROJ2CHK(err);

    thisOffset = newOffsets[rank];
    nSends = 0;
    for (int i = firstSend; i < lastSend; i++) {
      size_t sendStart = thisOffset;
      size_t sendEnd   = oldOffsets[i + 1];

      if (newOffsets[rank + 1] < sendEnd) {
        sendEnd = newOffsets[rank + 1];
      }
      if (sendEnd > sendStart) {
        err = MPI_Isend(&keysNew[thisOffset - newOffsets[rank]],(int) (sendEnd - sendStart), MPI_UINT64_T, i, PROJ2TAG_QUICKSORT, comm, &send_reqs[nSends++]); MPI_CHK(err);
      }
      thisOffset = sendEnd;
    }

    err = MPI_Waitall(nRecvs, recv_reqs, MPI_STATUSES_IGNORE); PROJ2CHK(err);
    err = MPI_Waitall(nSends, send_reqs, MPI_STATUSES_IGNORE); PROJ2CHK(err);

    err = Proj2SorterRestoreWorkArray(sorter, lastSend - firstSend, sizeof(*send_reqs), &send_reqs); PROJ2CHK(err);
    err = Proj2SorterRestoreWorkArray(sorter, lastRecv - firstRecv, sizeof(*recv_reqs), &recv_reqs); PROJ2CHK(err);
    err = Proj2SorterRestoreWorkArray(sorter, size + 1, sizeof(uint64_t), &newOffsets); PROJ2CHK(err);
    err = Proj2SorterRestoreWorkArray(sorter, size + 1, sizeof(uint64_t), &oldOffsets); PROJ2CHK(err);
  }
  err = Proj2SorterRestoreWorkArray(sorter, numKeysLocalNew, sizeof(uint64_t), &keysNew); PROJ2CHK(err);
  return 0;
}
int Proj2SorterSort_quicksort_single(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys)
{
  int err;
  int size;
  int rank;
  err = MPI_Comm_size(sorter->comm, &size); PROJ2CHK(err);
  err = MPI_Comm_rank(sorter->comm, &rank); PROJ2CHK(err);
  uint64_t* global_keys = NULL;
  if(rank == 0) {
    err = Proj2Malloc(numKeysLocal * size * sizeof(uint64_t), &global_keys); PROJ2CHK(err);
  }
  MPI_Gather(keys, numKeysLocal, MPI_UINT64_T, global_keys, numKeysLocal, MPI_UINT64_T, 0, sorter->comm);
  if(rank == 0) {
    err = Proj2SorterSortLocal(sorter, numKeysLocal*size, global_keys, PROJ2SORT_FORWARD);
  }
  MPI_Scatter(global_keys, numKeysLocal, MPI_UINT64_T, keys, numKeysLocal, MPI_UINT64_T, 0, sorter->comm);
  if(rank == 0) {
    Proj2Free(&global_keys);
  }

}

int Proj2SorterSort_quicksort(Proj2Sorter sorter, size_t numKeysLocal, uint64_t *keys)
{

  int err;
  int size;
  int rank;
  err = MPI_Comm_size(sorter->comm, &size); PROJ2CHK(err);
  err = MPI_Comm_rank(sorter->comm, &rank); PROJ2CHK(err);

    err = Proj2SorterSortLocal(sorter, numKeysLocal, keys, PROJ2SORT_FORWARD);
  PROJ2CHK(err);
  //find local regular p samples
  uint64_t* local_sample;
  err = Proj2Malloc(size * sizeof(uint64_t), &local_sample); PROJ2CHK(err);
  size_t w = numKeysLocal/size;
  for(int i = 0; i < size; ++i) {
    local_sample[i] = keys[i*w + 1]; 
  }
  uint64_t* global_sample = NULL;
  if(rank == 0) {
    //root processor gathers all samples
    err = Proj2Malloc(size * size * sizeof(uint64_t), &global_sample);PROJ2CHK(err);
  }
  MPI_Gather(local_sample, size, MPI_UINT64_T, global_sample, size, MPI_UINT64_T, 0, sorter->comm);
  //find pivots
  uint64_t* pivots;
  err = Proj2Malloc((size - 1) * sizeof(uint64_t), &pivots);PROJ2CHK(err);
  if(rank == 0) {
    err = Proj2SorterSortLocal(sorter, size*size, global_sample, PROJ2SORT_FORWARD);
    for(int i = 1; i < size; ++i) {
      pivots[i - 1] = global_sample[size * i];
    }
  }
  //broadcast pivots
  MPI_Bcast(pivots, size - 1, MPI_UINT64_T, 0, sorter->comm);
  
  //keys will be split into p partitions, each partition for a processor
  int* partition_ends;//index of the exclusing end of each partition
  err = Proj2Malloc(size * sizeof(int), &partition_ends); PROJ2CHK(err);
  partition_ends[size - 1] = numKeysLocal;
  //binary search
  //for each pivot[p] find the first element greater than pivot[p]
  for(int p = 0; p < size - 1;++p) {
    int start = 0;
    int end = numKeysLocal;
    while(start != end) {
      int mid = start + (end - start) / 2;
      if(keys[mid] <= pivots[p]) {
        start = mid + 1;
      } else {
        end = mid;
      }
    }
    partition_ends[p] = end;
  }

  int* send_size;//size of partitions
  err = Proj2Malloc(size * sizeof(int), &send_size); PROJ2CHK(err);

  int prev = 0;
  for(int p = 0; p < size; ++p) {
    send_size[p] = partition_ends[p] - prev;
    prev = partition_ends[p];
  }

  int* recv_size;//transpose of send_size
  err = Proj2Malloc(size * sizeof(int), &recv_size); PROJ2CHK(err);
  MPI_Alltoall(send_size, 1, MPI_INT, recv_size, 1, MPI_INT, sorter->comm);
  int* send_disp;//size of partitions
  err = Proj2Malloc(size * sizeof(int), &send_disp); PROJ2CHK(err);

  prev = 0;
  for(int p = 0; p < size; ++p) {
    send_disp[p] = prev;
    prev += send_size[p];
  }

  int* recv_disp;//size of partitions
  err = Proj2Malloc(size * sizeof(int), &recv_disp); PROJ2CHK(err);
  recv_disp = (int*) malloc(size*sizeof(int));
  prev = 0;
  for(int p = 0; p < size; ++p) {
    recv_disp[p] = prev;
    prev += recv_size[p];
  }
  int total_recv = prev;
  uint64_t* new_keys;//size of partitions
  err = Proj2Malloc(total_recv * sizeof(uint64_t), &new_keys); PROJ2CHK(err);
  //use alltoallv to send partition i to processor i
  MPI_Alltoallv(keys, send_size, send_disp, MPI_UINT64_T,
      new_keys, recv_size, recv_disp, MPI_UINT64_T,
      sorter->comm);

  //sort again
  //p-way sort could be better
  err = Proj2SorterSortLocal(sorter, total_recv, new_keys, PROJ2SORT_FORWARD);
    //redistribute
  //allgather newsize in each processor
  int* new_size;//size of partitions
  err = Proj2Malloc(size * sizeof(int), &new_size); PROJ2CHK(err);
  MPI_Allgather(&total_recv, 1, MPI_INT, new_size, 1, MPI_INT, sorter->comm);

  uint64_t* send_ends;//exclusing ending index in the global sorted array
  err = Proj2Malloc(size * sizeof(uint64_t), &send_ends); PROJ2CHK(err);
  uint64_t sum = 0;
  for(int p = 0; p < size; ++p) {
    sum += new_size[p];
    send_ends[p] = sum;
  }
  uint64_t* recv_ends;//exclusing ending index in the global sorted array
  err = Proj2Malloc(size * sizeof(uint64_t), &recv_ends); PROJ2CHK(err);
  for(int p = 0; p < size; ++p) {
    recv_ends[p] = (p+1)*numKeysLocal;
  }
  calculate_size_disp(rank,size,send_ends, recv_ends, send_size, send_disp);
  calculate_size_disp(rank,size,recv_ends, send_ends, recv_size, recv_disp);


  MPI_Alltoallv(new_keys, send_size, send_disp, MPI_UINT64_T,
      keys, recv_size, recv_disp, MPI_UINT64_T,
      sorter->comm);

  Proj2Free(&local_sample);
  if(rank == 0) {
    Proj2Free(&global_sample);
  } 
  Proj2Free(&pivots);
  Proj2Free(&partition_ends);
  Proj2Free(&send_size);
  Proj2Free(&recv_size);
  Proj2Free(&send_disp);
  Proj2Free(&recv_disp);
  Proj2Free(&new_keys);
  Proj2Free(&send_ends);
  Proj2Free(&recv_ends);
  return 0;
}
/* use to calculate send_size and send_disp when rebalancing the global array
 * input rank of processor, size of world, exclusing end  of local array(unbalanced), exclusing ends of local array(blanced)
 */
void calculate_size_disp(int rank, int size, uint64_t* send_ends, uint64_t* recv_ends, int* send_size, int* send_disp) {
  int displ = 0;
  uint64_t start;
  if(rank == 0) {
    start = 0;
  } else {
    start = send_ends[rank - 1];
  }
  uint64_t end = send_ends[rank];
  for(int p = 0; p < size; ++p) {
    if(start == end || start >= recv_ends[p]) {
      send_size[p] = 0;
      send_disp[p] = displ;
    } else {
      uint64_t new_end = end < recv_ends[p] ? end : recv_ends[p];
      int size = new_end - start;
      send_size[p] = size;
      send_disp[p] = displ;
      displ += size;
      start += size;
    }
  }
}
